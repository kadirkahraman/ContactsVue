const webpack = require("webpack");

module.exports = {
    devServer: {
        host: "localhost",
        port: 8088,
        progress: false
    },
    css: {
        extract: false
    },
    configureWebpack: {
        output: {
            filename: "vue/builded.js",
            chunkFilename: "vue/chunk-build.js",
            hotUpdateChunkFilename: "vue/hot/hot-update.[hash].js",
            hotUpdateMainFilename: "vue/hot/hot-update.[hash].json"
        }
    },
    outputDir: "target/front-end",
    pages: {
        index: {
            entry: "src/main.js",
            template: "public/index.html",
            filename: "index.html",
            title: "Main Page"
        }
    },
    lintOnSave: false,
};
