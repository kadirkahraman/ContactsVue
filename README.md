# Contacts Frontend

Contacts project's front-end section.

#### Run Configurations:
| Build Type | Command |
| ------ | ------ |
| Serve | npm run serve |
| Prod Build | npm run build |
| Lint | npm run lint | 