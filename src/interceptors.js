import Vue from 'vue';
import VueResource from 'vue-resource';

const interceptors = {
    bypassBrowserCache: (request, next) => {
        if (request.method === 'GET') {
            request.params['time'] = new Date().getTime();
        }
        next();
    },
    tokenAuthentication: (request, next) => {
        const token = Vue.$cookies.get("token");
        if (token) {
            request.headers.append("Authorization", `Basic ${token}`);
        }
        next();
    }
};

export default {
    configure() {
        Vue.use(VueResource);
        Vue.http.options.root = "http://localhost:8080";
        Vue.http.options.emulateJSON = true;
        Vue.http.options.emulateHTTP = true;
        Object.values(interceptors).forEach(interceptor => Vue.http.interceptors.push(interceptor));
    }
}
