import Users from './user/Users.vue';
import UserRoles from './roles/UserRoles.vue';
import UserView from "./user/UserView";

export default {
    routes: [
        {
            path: '/configurations/users',
            component: Users,
            meta: {
                visibleName: 'Users',
                privileges: ['ROLE_USER_ADMIN', 'ROLE_USER_VIEW', 'ROLE_SYSTEM_ADMIN'],
                iconClass: 'supervised_user_circle'
            }
        },
        {
            path: '/configurations/roles',
            component: UserRoles,
            meta: {
                visibleName: 'User Roles',
                privileges: ['ROLE_USER_ADMIN', 'ROLE_USER_VIEW', 'ROLE_SYSTEM_ADMIN'],
                iconClass: 'lock'
            }
        },
        {
            path: '/configurations/users/:id',
            component: UserView,
            meta: {
                privileges: ['ROLE_USER_ADMIN', 'ROLE_SYSTEM_ADMIN'],
                hidden: true
            }
        },
        {
            path: '*',
            redirect: '/configurations',
            meta: {
                hidden: true
            }
        },
        {
            path: '',
            redirect: '/configurations/users',
            meta: {
                hidden: true
            }
        }
    ]
};