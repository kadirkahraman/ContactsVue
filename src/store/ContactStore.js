import ContactService from "../service/ContactService";

const initializer = () => ({
    contacts: []
});

export default {
    namespaced: true,
    state: initializer(),
    mutations: {
        setContacts(state, contacts) {
            state.contacts = contacts;
        },
        setContact(state, contact) {
            state.contacts = state.contacts
                .filter(ct => ct.id !== contact.id)
                .concat(contact);
        },
        deleteContact(state, id) {
            state.contacts = state.contacts
                .filter(ct => ct.id !== id);
        }
    },
    actions: {
        refreshContacts({commit, dispatch}) {
            dispatch('authentication/refreshAuthentication', null, {root: true})
                .then(authResponse => {
                    if (!authResponse) {
                        return null;
                    }
                    commit("setContacts", authResponse.authenticatedUser.contacts);
                    return authResponse.authenticatedUser.contacts;
                });
        },
        saveContact({commit}, model) {
            let promise = null;
            if (!!model.id) {
                promise = ContactService.update(model);
            } else {
                promise = ContactService.create(model);
            }
            return promise.then(savedContact => {
                commit('setContact', model);
                return savedContact;
            });
        },
        deleteContact({commit}, id) {
            return ContactService.deleteContact(id)
                .then(deletedId => {
                    commit('deleteContact', deletedId);
                    return deletedId;
                });
        },
        findContact({}, id) {
            return ContactService.findContact(id).then(contact => contact);
        }
    },
    getters: {
        contactList(state) {
            return state.contacts;
        }
    }
}