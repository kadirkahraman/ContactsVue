import UserRoleService from "../service/UserRoleService";

const initializer = () => ({
    permissions: [],
    userRoles: []
});

export default {
    namespaced: true,
    state: initializer(),
    mutations: {
        setPermissions(state, permissions) {
            state.permissions = permissions;
        },
        setRoles(state, roles) {
            state.userRoles = roles;
        },
        setRole(state, userRole) {
            state.userRoles = state.userRoles
                .filter(role => role.id !== userRole.id);
        },
        deleteRole(state, id) {
            state.userRoles = state.userRoles
                .filter(role => role.id !== id);
        }
    },
    actions: {
        refreshPermissions({commit}) {
            return UserRoleService.permissions()
                .then(permissions => {
                    commit('setPermissions', permissions);
                    return permissions;
                });
        },
        refreshRoles({commit}) {
            return UserRoleService.list()
                .then(userRoles => {
                    commit('setRoles', userRoles);
                    return userRoles;
                });
        },
        saveRole({commit}, model) {
            let promise = null;
            if (!!model.id) {
                promise = UserRoleService.update(model);
            } else {
                promise = UserRoleService.create(model);
            }
            return promise.then(savedRole => {
                commit('setRole', model);
                return savedRole;
            });
        },
        deleteRole({commit}, id) {
            return UserRoleService.deleteRole(id)
                .then(deletedId => {
                    commit('deleteRole', deletedId);
                    return deletedId;
                });
        }
    },
    getters: {
        permissionList(state) {
            return state.permissions;
        },
        userRoleList(state) {
            return state.userRoles;
        }
    }
}