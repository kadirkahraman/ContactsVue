import AuthenticationService from './../service/AuthenticationService';
import Vue from 'vue';

const initializer = () => ({
    authenticatedUser: null,
    token: ''
});

export default {
    namespaced: true,
    state: initializer(),
    mutations: {
        setAuthenticatedUser(state, user) {
            state.authenticatedUser = user;
        },
        setToken(state, {token, tokenDetails}) {
            const sessionTime = tokenDetails.expireDate - new Date().getTime();
            Vue.$cookies.set("token", token, sessionTime);
            state.token = token;
        },
        clearAuthentication(state) {
            Vue.$cookies.remove("token");
            state.authenticatedUser = null;
            state.token = null;
        }
    },
    actions: {
        signUp({}, dataModel) {
            return AuthenticationService.signUp(dataModel)
                .then(user => user);
        },
        login({commit}, loginModel) {
            return AuthenticationService.login(loginModel)
                .then(user => {
                    commit('setAuthenticatedUser', user);
                    commit('setToken', {token: user.token, tokenDetails: user.tokenDetails});
                    return user;
                });
        },
        refreshAuthentication({commit, getters, dispatch}) {
            const token = getters['token'];
            if (!token) {
                console.debug("Token not found");
                return;
            }
            return AuthenticationService.refreshAuthentication(token)
                .then(user => {
                    commit('setAuthenticatedUser', user);
                    commit('setToken', {token: user.token, tokenDetails: user.tokenDetails});
                    return user;
                })
                .catch(e => {
                    if (e.status === 403) {
                        dispatch('logout');
                    }
                });
        },
        changePassword({}, model) {
            return AuthenticationService.changePassword(model);
        },
        logout({commit}) {
            commit('clearAuthentication');
        }
    },
    getters: {
        authenticatedUser(state) {
            return state.authenticatedUser;
        },
        token(state) {
            return state.token || Vue.$cookies.get("token");
        },
        authorization(state) {
            const token = state.token;
            return `Basic ${token}`;
        }
    }
}