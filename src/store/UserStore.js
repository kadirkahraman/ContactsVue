import UserService from "../service/UserService";

const initializer = () => ({
    users: []
});

export default {
    namespaced: true,
    state: initializer(),
    mutations: {
        setUsers(state, users) {
            state.users = users;
        },
        setUser(state, user) {
            state.users = state.users
                .filter(usr => usr.id !== user.id)
                .concat(user);
        },
        deleteUser(state, id) {
            state.users = state.users
                .filter(usr => usr.id !== id);
        }
    },
    actions: {
        refreshUsers({commit}) {
            return UserService.list()
                .then(users => {
                    commit('setUsers', users);
                    return users;
                });
        },
        saveUser({commit}, model) {
            let promise = null;
            if (!!model.id) {
                promise = UserService.update(model);
            } else {
                promise = UserService.create(model);
            }
            return promise.then(savedUser => {
                commit('setUser', model);
                return savedUser;
            });
        },
        deleteUser({commit}, id) {
            return UserService.deleteUser(id)
                .then(deletedId => {
                    commit('deleteUser', deletedId);
                    return deletedId;
                });
        }
    },
    getters: {
        userList(state) {
            return state.users;
        }
    }
}