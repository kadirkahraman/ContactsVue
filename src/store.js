import AuthenticationStore from './store/AuthenticationStore';
import UserRoleStore from "./store/UserRoleStore";
import UserStore from "./store/UserStore";
import ContactStore from "./store/ContactStore";

export default {
    modules: {
        authentication: AuthenticationStore,
        userRole: UserRoleStore,
        user: UserStore,
        contact:ContactStore
    }
}