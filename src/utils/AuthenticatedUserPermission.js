const permissionDirective = {
    bind(el, bind, node) {
        const permissions = [];
        if (bind.value) {
            if (Array.isArray(bind.value)) {
                bind.value.filter(str => typeof str === 'string').forEach(str => permissions.push(str));
            } else if (typeof bind.value === 'string') {
                permissions.push(bind.value);
            }
        } else {
            bind.expressions
                .split(',')
                .map(str => str.trim())
                .forEach(str => permissions.push(str));
        }
        node.context.permissionMixinCheckAuthChanges({element: el, permissions: permissions});
    },
    unbind(el, bind, node) {
        node.context.breakPermissionMixinCheckAuthChanges(el);
    }
};

const permissionMixin = {
    data() {
        return {
            permissionMixinDataUnwatchFunc: null,
            permissionMixinDataRelatedElements: []
        };
    },
    computed: {
        authenticatedUserObj() {
            const authResult = this.$store && this.$store.getters['authentication/authenticatedUser'];
            return authResult && authResult.authenticatedUser;
        }
    },
    methods: {
        permissionMixinCheckAuthChanges(item) {
            if (!this.$data.permissionMixinDataUnwatchFunc) {
                this.$data.permissionMixinDataUnwatchFunc = this.$watch('authenticatedUser', this.handleAuthChanges, {
                    deep: true,
                    immediate: true
                });
            }
            this.$data.permissionMixinDataRelatedElements.push(item);
            this.handleAuthChanges(this.authenticatedUserObj);
        },
        breakPermissionMixinCheckAuthChanges(el) {
            const elementIndex = this.$data.permissionMixinDataRelatedElements.findIndex(item => item.element === el);
            if (elementIndex < 0) {
                console.error("Couldn't find element " + el);
                return;
            }
            this.$data.permissionMixinDataRelatedElements.splice(elementIndex, 1);
            el.classList.remove("permission-invisible");
            if (this.$data.permissionMixinDataRelatedElements.length === 0) {
                this.$data.permissionMixinDataUnwatchFunc();
                this.$data.permissionMixinDataUnwatchFunc = null;
            }
        },
        handleAuthChanges(authUser) {
            if (!authUser) {
                this.$data.permissionMixinDataRelatedElements.forEach(item => item.element.classList.add('permission-invisible'));
            } else {
                this.$data.permissionMixinDataRelatedElements.forEach(item => {
                    const element = item.element;
                    const permissions = item.permissions;
                    const isPermitted = this.userHasMinOneOfPermissions(authUser, permissions);
                    if (isPermitted) {
                        element.classList.remove('permission-invisible');
                    } else {
                        element.classList.add('permission-invisible');
                    }
                });
            }
        },
        userHasMinOneOfPermissions(user, requiredPermissions) {
            if (!user || !user.userRole) {
                return false;
            }
            if (!requiredPermissions || requiredPermissions.length === 0) {
                return true;
            }
            for (const i in requiredPermissions) {
                if (!!user.userRole.permissions.find(p => p.enumName === requiredPermissions[i])) {
                    return true;
                }
            }
            return false;
        },
        userHasPermission(permission) {
            if (!permission || (Array.isArray(permission) && !permission.length)) {
                return true;
            }
            const user = this.authenticatedUserObj;
            if (typeof permission === 'string' && permission.indexOf(',') < 0) {
                return this.userHasMinOneOfPermissions(user, [permission]);
            } else if (typeof permission === 'string' && permission.indexOf(',') >= 0) {
                const permissions = Array.from(permission.split(',').map(s => s.trim));
                return this.userHasMinOneOfPermissions(user, permissions);
            } else if (Array.isArray(permission)) {
                return this.userHasMinOneOfPermissions(user, permission);
            }
            console.error("userHasPermission failed");
        }
    }
};

export {permissionDirective, permissionMixin};