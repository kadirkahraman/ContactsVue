import Vue from 'vue';
import {permissionMixin} from './utils/AuthenticatedUserPermission';

export default{
    configure() {
        Vue.mixin(permissionMixin);
    }
}