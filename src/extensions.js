const setWindowApplication = (configuredVue) => {
    window.__vueGlobal__ = configuredVue;
};

export default {
    configure(configuredVue) {
        setWindowApplication(configuredVue);
    }
}
