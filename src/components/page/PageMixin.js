export default {
    data() {
        return {
            errorMessage: ''
        }
    },
    methods: {
        clearErrorMessage() {
            this.errorMessage = '';
        },
        addError(error) {
            if (error instanceof Object) {
                this.errorMessage = error.errorMessage || error.message || error.body || error.bodyText
            } else if (Array.isArray(error)) {
                this.errorMessage = error.toString();
            } else {
                this.errorMessage = error;
            }
        },
        generateEmptyPageContent(icon, label, description) {
            return {
                icon: icon,
                label: label,
                description: description
            };
        },
        generateEmptyPageActionButtonUtil(icon, label) {
            return {
                icon: icon,
                label: label
            }
        }
    }
}