export default {
    props:{
        rowId: {
            type: [Object, Number, String],
            required: true
        }
    },
    methods: {
        editClicked() {
            this.$emit("editClicked", this.rowId);
        },
        beforeProcessStart() {
            this.$emit('beforeProcessStart', this.rowId);
        },
        processCompleted() {
            this.$emit('processCompleted', this.rowId);
        },
        processFailed(error) {
            this.$emit('processFailed', {rowId: this.rowId, error});
        }
    }
}