export default {
    props: {
        routes: {
            type: Array,
            required: true
        },
        exact: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    methods: {
        pathIcon(meta) {
            if (meta && meta.iconClass) {
                return meta.iconClass;
            }
            return '';
        }
    }
};