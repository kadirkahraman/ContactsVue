import Contacts from "./views/contacts/Contacts";
import Configurations from "./views/configurations/Configurations";
import ConfigurationRoutes from "./views/configurations/ConfigurationRoutes";
import ContactView from "./views/contacts/ContactView";

export default {
    routes: [
        {
            path: '/contacts',
            component: Contacts,
            meta: {
                visibleName: 'Contacts',
                privileges: [],
                iconClass: 'contacts',
                hidden: false
            }
        },
        {
            path: '/configurations',
            component: Configurations,
            meta: {
                visibleName: 'Configurations',
                privileges: [],
                iconClass: 'build',
                hidden: false
            },
            children: ConfigurationRoutes.routes
        },
        {
            path: '/contacts/:id',
            component: ContactView,
            meta: {
                privileges: ['ROLE_CONTACT_ADMIN', 'ROLE_CONTACT_ADMIN'],
                hidden: true
            }
        },
        {
            path: '*',
            redirect: '/',
            meta: {
                hidden: true
            }
        },
        {
            path: '',
            redirect: '/contacts',
            meta: {
                hidden: true
            }
        }
    ]
}