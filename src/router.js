import ApplicationRoutes from './ApplicationRoutes';
import FilteredRouterView from './components/route/FilteredRouterView.vue';

const mapForPermissionPerRoute = (route) => {
    if (!route.meta) {
        return route.meta = {};
    }

    route.meta.routerPage = route.component;
    route.component = FilteredRouterView;

    if (route.children) {
        route.children = route.children.map(mapForPermissionPerRoute);
    }
    return route;
};

export default {
    activateRoutes(router) {
        router.addRoutes(ApplicationRoutes.routes.map(mapForPermissionPerRoute));
    }
};