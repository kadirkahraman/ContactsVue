import Vue from 'vue';
import Vuex from 'vuex';
import VueMaterial from "vue-material";
import VueRouter from 'vue-router';
import VueCookies from 'vue-cookies';

import App from './App.vue';
import store from './store';
import interceptors from "./interceptors";
import extensions from "./extensions";
import router from "./router";
import directives from "./directives";
import mixin from "./mixin";

import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default-dark.css';

Vue.config.productionTip = false;

Vue.use(VueMaterial);
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueCookies);

Vue.material.locale.dateFormat = "dd/MM/yyyy";
Vue.material.locale.firstDayOfAWeek = 1;

directives.configure();
interceptors.configure();
mixin.configure();

const application = new Vue({
    el: "#app",
    router: new VueRouter(router),
    store: new Vuex.Store(store),
    render: h => h(App)
});

extensions.configure(application);