import Vue from 'vue';
import {permissionDirective} from './utils/AuthenticatedUserPermission';

export default {
    configure() {
        Vue.directive('permission', permissionDirective);
    }
}