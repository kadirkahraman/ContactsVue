import Vue from 'vue';

export default {
    signUp(dataModel) {
        return Vue.http.post("api/auth/signUp", dataModel)
            .then(response => response.body);
    },
    login(loginModel) {
        return Vue.http.post("api/auth/login", loginModel, {
            emulateJSON: false,
            emulateHTTP: false
        }).then(response => response.body);
    },
    refreshAuthentication(token) {
        return Vue.http.post("api/auth/me", {token})
            .then(response => response.body);
    },
    changePassword(model) {
        return Vue.http.post("api/auth/updatePassword", model)
            .then(response => response.body);
    }
}