import Vue from 'vue';

export default {
    permissions() {
        return Vue.http.get("api/userrole/permissions")
            .then(response => response.body);
    },
    list() {
        return Vue.http.get("api/userrole")
            .then(response => response.body);
    },
    create(model) {
        return Vue.http.post("api/userrole", model)
            .then(response => response.body);
    },
    update(model) {
        return Vue.http.put("api/userrole", model)
            .then(response => response.body);
    },
    deleteRole(id) {
        return Vue.http.delete(`api/userrole/${id}`)
            .then(response => response.body);
    }
}