import Vue from 'vue';

export default {
    list() {
        return Vue.http.get("api/user")
            .then(response => response.body);
    },
    create(model) {
        return Vue.http.post("api/user", model)
            .then(response => response.body);
    },
    update(model) {
        return Vue.http.put("api/user", model)
            .then(response => response.body);
    },
    deleteUser(id) {
        return Vue.http.delete(`api/user/${id}`)
            .then(response => response.body);
    }
}