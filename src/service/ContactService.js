import Vue from 'vue';

export default {
    create(model) {
        return Vue.http.post("api/contact", model)
            .then(response => response.body);
    },
    update(model) {
        return Vue.http.put("api/contact", model)
            .then(response => response.body);
    },
    deleteContact(id) {
        return Vue.http.delete(`api/contact/${id}`)
            .then(response => response.body);
    },
    findContact(id) {
        return Vue.http.get(`api/contact/${id}`)
            .then(response => response.body);
    }
}